package com.daniela.automation.users.features.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.rest.SerenityRest;

import static net.serenitybdd.rest.SerenityRest.*;
import static org.hamcrest.Matchers.*;

//Validaciones para cada página
public class userSteps {

    @Given("^que es un usuario autorizado$")
    public void que_es_un_usuario_autorizado() throws Throwable {
        given().contentType("application/json");

    }

    @When("^solicita información de los usuarios de la URI página 1$")
    public void solicita_información_de_los_usuarios_de_la_URI_página_1() throws Throwable {
        when().get("https://reqres.in/api/users?page=1");
    }

    @Then("^debería obtener información de forma satisfactoria de la página 1$")
    public void debería_obtener_información_de_forma_satisfactoria_página_1() throws Throwable {
        then().statusCode(200).and().body(containsString("Janet")).and().body("per_page",equalTo(3));
    }


    @When("^solicita información de los usuarios de la URI página 2$")
    public void solicita_información_de_los_usuarios_de_la_URI_página_2() throws Throwable {
        when().get("https://reqres.in/api/users?page=2");
    }

    @Then("^debería obtener información de forma satisfactoria de la página 2$")
    public void debería_obtener_información_de_forma_satisfactoria_página_2() throws Throwable {
        then().statusCode(200).and().body(containsString("Charles")).and().body("per_page",equalTo(3));
    }


    @When("^solicita información de los usuarios de la URI página 3$")
    public void solicita_información_de_los_usuarios_de_la_URI_página_3() throws Throwable {
        when().get("https://reqres.in/api/users?page=3");
    }

    @Then("^debería obtener información de forma satisfactoria de la página 3$")
    public void debería_obtener_información_de_forma_satisfactoria_página_3() throws Throwable {
        then().statusCode(200).and().body(containsString("Lindsay")).and().body("per_page",equalTo(3));
    }


    @When("^solicita información de los usuarios de la URI página 4$")
    public void solicita_información_de_los_usuarios_de_la_URI_página_4() throws Throwable {
        when().get("https://reqres.in/api/users?page=4");
    }

    @Then("^debería obtener información de forma satisfactoria de la página 4$")
    public void debería_obtener_información_de_forma_satisfactoria_página_4() throws Throwable {
        then().statusCode(200).and().body(containsString("George")).and().body("per_page",equalTo(3));
    }

}
