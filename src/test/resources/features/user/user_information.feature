Feature: Consultar información de los usuarios

Scenario: Información de los usuarios de la página 1
 Given que es un usuario autorizado
 When solicita información de los usuarios de la URI página 1
 Then debería obtener información de forma satisfactoria de la página 1


Scenario: Información de los usuarios de la página 2
 Given que es un usuario autorizado
 When solicita información de los usuarios de la URI página 2
 Then debería obtener información de forma satisfactoria de la página 2


Scenario: Información de los usuarios de la página 3
 Given que es un usuario autorizado
 When solicita información de los usuarios de la URI página 3
 Then debería obtener información de forma satisfactoria de la página 3


Scenario: Información de los usuarios de la página 4
 Given que es un usuario autorizado
 When solicita información de los usuarios de la URI página 4
 Then debería obtener información de forma satisfactoria de la página 4


