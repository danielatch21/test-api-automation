# Prueba de automatización Api

## ¿Que contiene este repositorio?
Este repositorio contiene la prueba de automatización de api (URI: https://reqres.in/api/users?page=1).

## ¿Con quién se puede hablar acerca de este repositorio?
Ingeniera de calidad de software: Daniela Tobón

## Requerimientos para ejecutar las pruebas automatizadas:

1. Clonar este repositorio.
2. Compilar el proyecto "mvn clean install".
3. Para correr los test ejecutar el comando "mvn verify".

El resultado de los test se pueden visualizar en la consola.